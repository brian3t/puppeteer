const axios = require('axios');
const fetch = require('node-fetch');
const jq = require('jquery')
const _ = require('lodash');
const moment = require('moment');
const puppeteer = require('puppeteer');
const pageExtend = require('puppeteer-jquery')


// const DEBUG = false
const DEBUG = true
const DEBUG_MAX_BANDS = 1
// const API_URL = 'http://api.lnoapi/v1/'
// const API_URL = 'https://api.lnoapi/v1/'
const ppt_opt_debug = {
  headless: false,
  slowMo: 0,
  devtools: true
};

(async () => {
  // const query_url = `${API_URL}band?cols=id,name,ytlink_first,youtube&qr=` + JSON.stringify([['not', {ytlink_first: null}], {ytlink_first_tnail: null}])
  /** @type {Browser} **/
    // let bands = await axios.get(query_url)
    // if (! bands.data) return `API error`
    // bands = bands.data
    // if (! _.isArray(bands)) return `Need bands`
    // if (DEBUG) bands = bands.slice(0, DEBUG_MAX_BANDS)
    // console.log(`bands: `, bands)
  const browser = await puppeteer.launch(ppt_opt_debug)
  const pageOrg = await browser.newPage()
  let page = pageExtend.pageExtend(pageOrg)
  const now = (new moment()).format('MMDD_HHmmss')

  let is_thumb_valid, update_url, i = 0
  await page.goto(`https://soccercatch.com`)
  // await page.jQuery('body').prepend(`<h1>Brian Title</h1>`);

  // ytlink_first_tnail = await page.$eval("head > meta[property='og:image']", element => element.content)
  // is_thumb_valid = await fetch(ytlink_first_tnail, {method: 'HEAD'})
  // is_thumb_valid = is_thumb_valid?.ok
  //grab premier league matches
  page.on("console", consoleObj => console.log(consoleObj.text()));
  let pl_header_a = await page.jQuery(`a[href="/leagues/8/premier-league"]`);
  let last_anchor = pl_header_a[2]
  // let match_list_content_pl = page.$$('section.)
  last_anchor = page.jQuery(last_anchor)

  let section = last_anchor.parentsUntil('section')

  if (! DEBUG) await browser.close();
})();
